import { WebsocketApplication } from "@loopback4/websocket";
import { Component } from '@loopback/core';
import { TodoController } from './controllers';
import { Todo } from './models';
import { TodoRepository } from './repositories';
export declare class TodoComponent implements Component {
    private application;
    controllers: (typeof TodoController)[];
    models: (typeof Todo)[];
    repositories: (typeof TodoRepository)[];
    constructor(application: WebsocketApplication);
}
