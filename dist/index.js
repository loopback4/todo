"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodoComponent = void 0;
const tslib_1 = require("tslib");
const websocket_1 = require("@loopback4/websocket");
const core_1 = require("@loopback/core");
const controllers_1 = require("./controllers");
const models_1 = require("./models");
const repositories_1 = require("./repositories");
let TodoComponent = class TodoComponent {
    constructor(application) {
        this.application = application;
        this.controllers = [controllers_1.TodoController];
        this.models = [models_1.Todo];
        this.repositories = [repositories_1.TodoRepository];
    }
};
TodoComponent = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)(core_1.CoreBindings.APPLICATION_INSTANCE)),
    tslib_1.__metadata("design:paramtypes", [websocket_1.WebsocketApplication])
], TodoComponent);
exports.TodoComponent = TodoComponent;
//# sourceMappingURL=index.js.map