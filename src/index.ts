import {WebsocketApplication} from "@loopback4/websocket";
import { Component, CoreBindings, inject } from '@loopback/core';
import { TodoController } from './controllers';
import { Todo } from './models';
import { TodoRepository } from './repositories';

export class TodoComponent implements Component {
    controllers = [TodoController];
    models = [Todo];
    repositories = [TodoRepository];

    constructor(
        @inject(CoreBindings.APPLICATION_INSTANCE) private application: WebsocketApplication,
    ) {
    }
}
